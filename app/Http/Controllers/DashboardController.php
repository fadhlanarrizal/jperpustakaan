<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DashboardController extends Controller
{
    public function index()
    {
        $title = "Dashboard";
        $visitors = "1,225";
        $subs = "990";
        $bors = "1,334";
        $books = "576";
        $top_books = [[
            'buku' => 'Bumi',
            'penulis' => 'Tere Liye',
            'cover' => 'bumi.jpg',
            'borrowed' => '1.100k'
        ],
        [
            'buku' => 'Parable',
            'penulis' => 'Brian Khrisna',
            'cover' => 'parable.jpg',
            'borrowed' => '109k'
        ],
        [
            'buku' => 'Laut Bercerita',
            'penulis' => 'Leila Salikha Chudori',
            'cover' => 'laut_bercerita.jpg',
            'borrowed' => '80k'
        ]];
        $top_borrowers = [
            [
                'nama' => 'Jimmy Dennis',
                'status' => 'Pelajar',
                'foto_profil' => 'talha.jpg'
            ],
            [
                'nama' => 'John Doe',
                'status' => 'Mahasiswa',
                'foto_profil' => 'arashmil.jpg'
            ],
            [
                'nama' => 'James Reece',
                'status' => 'Tentara',
                'foto_profil' => 'sauro.jpg'
            ],
            [
                'nama' => 'Poy',
                'status' => 'Penyair',
                'foto_profil' => 'profile.jpg'
            ],
            [
                'nama' => 'Subhan Subekti',
                'status' => 'Programmer',
                'foto_profil' => 'mlane.jpg'
            ],
        ];
        $quote = [
                'inggris' => 'The more that you read, the more things you will know. The more that you learn, the more places you’ll go.',

                'indonesia' => 'Semakin banyak Anda membaca, semakin banyak hal yang akan Anda ketahui. Semakin banyak yang Anda pelajari, semakin banyak tempat yang akan Anda kunjungi.',

                'author' => 'Dr. Seuss'
            ];
            
        return view('dashboard.index', compact('title', 'visitors', 'subs', 'bors', 'books', 'top_books', 'top_borrowers', 'quote'));
    }
}
