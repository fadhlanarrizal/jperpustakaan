@section('dashboard', 'active')
    
<x-layouts.main>
<div class="row">

    <x-card class="card card-stats card-round card-primary">
        <div class="row">
            <div class="col-5">
                <div class="icon-big text-center">
                    <i class="icon-eye"></i>
                </div>
            </div>
            <div class="col-7 col-stats">
                <div class="numbers">
                    <p class="card-category">Visitors</p>
                    <h4 class="card-title">{{ $visitors }}</h4>
                </div>
            </div>
        </div>
    </x-card>
    
    <x-card class="card card-stats card-round card-success">
        <div class="row">
            <div class="col-5">
                <div class="icon-big text-center">
                    <i class="icon-heart"></i>
                </div>
            </div>
            <div class="col-7 col-stats">
                <div class="numbers">
                    <p class="card-category">Subscribers</p>
                    <h4 class="card-title">{{ $subs }}</h4>
                </div>
            </div>
        </div>
    </x-card>
    <x-card class="card card-stats card-round card-secondary">
        <div class="row">
            <div class="col-5">
                <div class="icon-big text-center">
                    <i class="icon-basket-loaded"></i>
                </div>
            </div>
            <div class="col-7 col-stats">
                <div class="numbers">
                    <p class="card-category">Borrowers</p>
                    <h4 class="card-title">{{ $bors }}</h4>
                </div>
            </div>
        </div>
    </x-card>
    <x-card class="card card-stats card-round card-info">
        <div class="row">
            <div class="col-5">
                <div class="icon-big text-center">
                    <i class="icon-notebook"></i>
                </div>
            </div>
            <div class="col-7 col-stats">
                <div class="numbers">
                    <p class="card-category">Books</p>
                    <h4 class="card-title">{{ $books }}</h4>
                </div>
            </div>
        </div>
    </x-card>

</div>

<div class="row">
    <x-card-black class="col-md-4">
        <div class="card-header">
            <div class="card-title">Top Books</div>
        </div>
        <div class="card-body pb-0">
            @foreach ($top_books as $buku)
                
            <div class="d-flex">
                    <img src="{{ asset('img/books/' . $buku['cover']) }}" alt="..." class="img-fluid rounded" style="width:50px;">
                <div class="flex-1 pt-1 ml-2">
                    <h6 class="fw-bold mb-1">{{ $buku['buku'] }}</h6>
                    <small class="text-muted">{{ $buku['penulis'] }}</small>
                </div>
                <div class="d-flex ml-auto align-items-center">
                    <h3 class="text-info fw-bold">{{ "+" . $buku['borrowed'] }}</h3>
                </div>
            </div>
            <div class="separator-dashed"></div>
            @endforeach
        </div>
    </x-card-black>

    <x-card-black class="col-md-4">
        <div class="card-body">
        <div class="card-title fw-mediumbold">Top Borrowers</div>
            <div class="card-list">
                @foreach ($top_borrowers as $borrower)
                    
                <div class="item-list">
                    <div class="avatar">
                        <img src="{{ asset('img/' . $borrower['foto_profil']) }}" alt="..." class="avatar-img rounded-circle">
                    </div>
                    <div class="info-user ml-3">
                        <div class="username">{{ $borrower['nama'] }}</div>
                        <div class="status">{{ $borrower['status'] }}</div>
                    </div>
                    <button class="btn btn-icon btn-primary btn-round btn-xs">
                        <i class="fa fa-plus"></i>
                    </button>
                </div>
                @endforeach
            </div>
        </div>

    </x-card-black>

    <x-card-black class="col-md-4">
        <div class="text-white" >
            <div class="card-body p-5">
  
              <i class="fas fa-quote-left fa-2x mb-4"></i>
  
              <p class="lead">{{ $quote['inggris'] }}</p>
  
              <hr>
  
              <div class="d-flex justify-content-between">
                <p class="mb-0">{{ $quote['author'] }}</p>
              </div>
  
            </div>
          </div>
    </x-card-black>
</div>

<div class="row mt-4">
    <x-chart></x-chart>
    <x-aktifitas></x-aktifitas>
</div>

</x-layouts.main>