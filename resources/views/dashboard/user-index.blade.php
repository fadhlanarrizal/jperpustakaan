<x-layouts.main>

    <x-card-black class="col">
        <div class="row p-3">
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="card-body p-0">
                    <div class="d-flex align-items-center">
                        <div class="col-6 p-0 position-relative image-overlap-shadow">
                            <a href="javascript:void();"><img class="img-fluid rounded w-100"
                                    src="{{ asset('img/books/bumi.jpg') }}" alt=""></a>
                        </div>
                        <div class="col-6">
                            <div class="mb-2">
                                <h6 class="mb-1">Bumi</h6>
                                <p class="font-size-13 line-height mb-1">Tere Liye</p>
                                <div class="d-block line-height">
                                    <span class="font-size-11 text-warning">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </span>
                                </div>
                                <div class="view-book">
                                    <a href="book-page.html" class="btn btn-sm btn-white my-4">View Book</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="card-body p-0">
                    <div class="d-flex align-items-center">
                        <div class="col-6 p-0 position-relative image-overlap-shadow">
                            <a href="javascript:void();"><img class="img-fluid rounded w-100"
                                    src="{{ asset('img/books/bumi.jpg') }}" alt=""></a>
                        </div>
                        <div class="col-6">
                            <div class="mb-2">
                                <h6 class="mb-1">Bumi</h6>
                                <p class="font-size-13 line-height mb-1">Tere Liye</p>
                                <div class="d-block line-height">
                                    <span class="font-size-11 text-warning">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </span>
                                </div>
                                <div class="view-book">
                                    <a href="book-page.html" class="btn btn-sm btn-white my-4">View Book</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="card-body p-0">
                    <div class="d-flex align-items-center">
                        <div class="col-6 p-0 position-relative image-overlap-shadow">
                            <a href="javascript:void();"><img class="img-fluid rounded w-100"
                                    src="{{ asset('img/books/bumi.jpg') }}" alt=""></a>
                        </div>
                        <div class="col-6">
                            <div class="mb-2">
                                <h6 class="mb-1">Bumi</h6>
                                <p class="font-size-13 line-height mb-1">Tere Liye</p>
                                <div class="d-block line-height">
                                    <span class="font-size-11 text-warning">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </span>
                                </div>
                                <div class="view-book">
                                    <a href="book-page.html" class="btn btn-sm btn-white my-4">View Book</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-3">
                <div class="card-body p-0">
                    <div class="d-flex align-items-center">
                        <div class="col-6 p-0 position-relative image-overlap-shadow">
                            <a href="javascript:void();"><img class="img-fluid rounded w-100"
                                    src="{{ asset('img/books/bumi.jpg') }}" alt=""></a>
                        </div>
                        <div class="col-6">
                            <div class="mb-2">
                                <h6 class="mb-1">Bumi</h6>
                                <p class="font-size-13 line-height mb-1">Tere Liye</p>
                                <div class="d-block line-height">
                                    <span class="font-size-11 text-warning">
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                        <i class="fa fa-star"></i>
                                    </span>
                                </div>
                                <div class="view-book">
                                    <a href="book-page.html" class="btn btn-sm btn-white my-4">View Book</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </x-card-black>

</x-layouts.main>
