@section('daftar_anggota', 'active')
    
<x-layouts.main>
    <x-table>
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>NIK</th>
                <th>email</th>
                <th>Alamat</th>
                <th>No Telp</th>
                <th>Aksi</th>
            </tr>
        <tbody>
        </tbody>
        </thead>
        @php
            $i = 1;
        @endphp
        @foreach ($anggotas as $anggota)
            <tr>
                <td>{{ $i++ }}</td>
                <td>{{ $anggota['nama'] }}</td>
                <td>{{ $anggota['NIK'] }}</td>
                <td>{{ $anggota['email'] }}</td>
                <td>{{ $anggota['alamat'] }}</td>
                <td>{{ $anggota['no_telp'] }}</td>
                <td class="col-md-2">
                    <p class="flex">
                        <button class="btn btn-success"><a href="" class="text-white">Edit</a></button>
                        <button class="btn btn-danger"><a href="" class="text-white">Hapus</a></button>
                    </p>
                </td>
            </tr>
        @endforeach
    </x-table>
</x-layouts.main>
