<!DOCTYPE html>
<html lang="en">

<head>

    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <title>JPerpus | {{  $title ?? config('app.name') }}</title>
    <meta content='width=device-width, initial-scale=1.0, shrink-to-fit=no' name='viewport' />
    @include('components.layouts.partials.styles')

</head>

<body data-background-color="dark">
    <div class="wrapper">

        @include('components.navbar')

        @include('components.sidebar')

        <div class="main-panel">
            <div class="content">
                <div class="container mt-3">

                    {{ $slot }}

                </div>
            </div>
        </div>
    </div>

    @include('components.layouts.partials.footer')

    @include('components.layouts.partials.scripts')
</body>

</html>
