    <div class="col-sm-6 col-md-3">
        <div {{ $attributes }}>
            <div class="card-body">
                {{ $slot }}
            </div>
        </div>
    </div>
