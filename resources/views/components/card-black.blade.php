<div {{ $attributes }}>
    <div class="card">
        {{ $slot }}
    </div>
</div>